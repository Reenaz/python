import urllib.request
import os
import time
from html_parser import file_name_parser

def url_file_loader(URL, file_name = None):
    if(file_name is None):
        file_name = file_name_parser(URL)
    print("Starting download "+file_name)
    urllib.request.urlretrieve(URL, file_name)
    print("Done.")




