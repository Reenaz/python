import urllib.request
import bs4

def link_parser(URL):
    all_links = []
    page = urllib.request.urlopen(URL)

    soup = bs4.BeautifulSoup(page, "html.parser")

    images = soup.find_all("img")
    for image in images:
        all_links.append(image['src'])

    css_files = soup.find_all(attrs={"type": "text/css", "rel": "stylesheet"})
    for css in css_files:
        all_links.append(css['href'])

    js_files = soup.find_all(attrs={"type": "text/javascript"})
    for js in js_files:
        try:
            all_links.append(js['src'])
        except KeyError:
            continue

    for files in all_links:
        print(files)

    return all_links




def file_name_parser(url):
    if (url[len(url)-1] == "/"):
        url = url[0:len(url)-1]
    return url[url.rfind("/") + 1:len(url)]


