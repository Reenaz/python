import os
import time
from html_parser import link_parser
from html_parser import file_name_parser
from file_loader import url_file_loader



def URL_loader(URL, file_name = None):
    if file_name is None:
        file_name = URL[URL.rfind("/")+1:len(URL)]

    if file_name == "":
        file_name = "page" + str(time.time())


    if (os.path.exists(os.path.abspath(os.getcwd())+"\\"+file_name) != True):
        os.mkdir(file_name)
    os.chdir(os.path.abspath(os.getcwd())+"\\"+file_name)

    start_path = os.path.abspath(os.getcwd())

    url_file_loader(URL)

    os.chdir("C:/")
    start_path = "C:/"

    links = link_parser(URL)

    for link in links:
        if(link[0] == "/"):
            end = link.rfind("/")
            path = start_path + link[0:end]
            if(os.path.exists(path)):
                continue
            os.makedirs(path)
            os.chdir(path)
            link = URL + link
        url_file_loader(link)






